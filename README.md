Fedora post-install scripts
==========================

This script has been written to be launched without using sudo or root. Download the repository and extract to your home folder.
For correct permissions and ownership, run:

`sh fedora-post-install.sh`


Alternatively you can use this one line command to download and execute the script:

`wget -c https://gitlab.com/nooryani84/fedora/-/archive/master/fedora-master.tar.gz -O - | tar -xzv --one-top-level=fedora-post-install --strip-components 1 && cd fedora-post-install && sh fedora-post-install.sh`

For details on what the scripts do, see the [Scripts](https://gitlab.com/nooryani84/fedora/-/tree/master/scripts) folder.
