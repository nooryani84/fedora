#!/bin/bash
#	set options explained:
#	'-e'		Stops the script if a command fails
#	'-u'		Any variable you haven't previously defined other than the special parameters ‘@’ or ‘*’
#			or array variables subscripted with ‘@’ or ‘*’ will cause an error that stops the script
#	'-x'            Causes bash to print each command before executing it.
#	'-o pipefail' 	If any command in a pipeline fails, the script stops
set -eo pipefail
#
################################################################################################################
# Define colors for output text
red=$(tput setaf 1)
normal=$(tput sgr0)

# Check if user is running as root
if [ "$(id -u)" = "0" ]; then
  printf "${red}This script must be run as non-root.${normal}\n"
exit 1
fi
################################################################################################################

REL_SCRIPT_ROOTDIR="$(dirname $0)"
SCRIPT_ROOTDIR="$(realpath $REL_SCRIPT_ROOTDIR)"
SCRIPT_SUBDIR="$(realpath $SCRIPT_ROOTDIR/scripts)"
LOGS="$SCRIPT_ROOTDIR/logs"

# Run the main script which will prompt for sudo
  sudo sh $SCRIPT_SUBDIR/00main.sh 2>&1 | tee $LOGS/00main.log

# Tasks to be performed as non-root user
  sh $SCRIPT_SUBDIR/non-root.sh | tee $LOGS/non-root.log

# GNOME customizations for touchpad - uncomment if using laptop with touchpad
# sh $SCRIPT_SUBDIR/gsettings-laptop.sh

printf "\n"
printf "#######################\n"
printf "# ${red}You should restart.${normal} #\n"
printf "#######################\n"
printf "\n"
printf "Would you like to restart now?\n"
printf "\n"

while true; do
read -p "(y/n)" choice
case $choice in
  y|Y ) systemctl reboot -i && rm -R $SCRIPT_ROOTDIR;;
    # Comment out line above and uncomment line below to keep files for debugging
	  #y|Y ) systemctl reboot -i
  n|N ) break;;
esac
done
