#!/bin/bash
#	set options explained:
#	'-e'		Stops the script if a command fails
#	'-u'		Any variable you haven't previously defined other than the special parameters ‘@’ or ‘*’
#			or array variables subscripted with ‘@’ or ‘*’ will cause an error that stops the script
#	'-x'            Causes bash to print each command before executing it.
set -e
#
################################################################################################################
# Define colors for output text
red=$(tput setaf 1)
normal=$(tput sgr0)

# Check if user is running as root
if [ "$(id -u)" != "0" ]; then
  printf "${red}This script must be run as root.${normal}\n"
exit 1
fi


# Start off with RPi OS lite

# Adds the following to the config.txt - you must reboot for changes to take effect
## Overclock
#arm_freq=3000
#over_voltage_delta=50000
#gpu_freq=1000

sudo sed -i '$a\\n# Overclock\narm_freq=3000\nover_voltage_delta=50000\ngpu_freq=1000' /boot/firmware/config.txt

# Disable source repositories
sudo sed -i -e 's/deb-src/#deb-src/g' /etc/apt/sources.list

# Update repos and upgrade Ubuntu
sudo apt-get -y update

KDE="ark dolphin gwenview kcalc kde-config-flatpak kde-spectacle kdialog keditbookmarks kfind konsole \
kwrite okular plasma-desktop plasma-discover-backend-flatpak plasma-workspace sddm"

APPS="command-not-found curl firefox udisks2 upower"

# Install KDE and other apps.
sudo apt-get install -y \
$KDE $APPS

# Remove kdeconnect
sudo apt-get autoremove -y --purge \
kdeconnect

# Set the default target, enabling SDDM as the display manager 
sudo systemctl set-default graphical.target

# Add Flathub as a Flatpak repository
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

printf "\n"
printf "#######################\n"
printf "# ${red}You should restart.${normal} #\n"
printf "#######################\n"
printf "\n"
printf "Would you like to restart now?\n"
printf "\n"

while true; do
read -p "(y/n)" choice
case $choice in
  y|Y ) systemctl reboot -i;;
  n|N ) break;;
esac
done

