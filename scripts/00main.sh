#!/bin/bash
#	set options explained:
#	'-e'		Stops the script if a command fails
#	'-u'		Any variable you haven't previously defined other than the special parameters ‘@’ or ‘*’
#			or array variables subscripted with ‘@’ or ‘*’ will cause an error that stops the script
#	'-x'            Causes bash to print each command before executing it.
#	'-o pipefail' 	If any command in a pipeline fails, the script stops
set -eo pipefail
#
#################################################
# Script designed to be run with sudo           #
# for correct permissions and ownership, run:   #
# sudo sh fedora.sh                             #
#################################################
#
# To download Fedora sub-folder,extract, change to folder and run script via terminal:
# wget -c https://gitlab.com/nooryani84/fedora/-/archive/master/fedora-master.tar.gz -O - | tar -xzv --one-top-level=fedora-post-install --strip-components 1 && cd ~/fedora-post-install && sudo sh fedora-post-install.sh
################################################################################################################
# Define colors for output text
red=$(tput setaf 1)
cyan=$(tput setaf 6)
normal=$(tput sgr0)

# Check if user is running script as root
if [ "$(id -u)" != "0" ]; then
   printf "${red}This script must be run as root.${normal}\n"
exit 1
fi
################################################################################################################

clear

REL_SCRIPT_SUBDIR="$(dirname $0)"
SCRIPT_SUBDIR="$(realpath $REL_SCRIPT_SUBDIR)"
LOGS="$(dirname "$SCRIPT_SUBDIR")/logs"

# DNF: Remove applications
# Comment out for Silverblue
   printf "\n"
	printf "${cyan}Removing applications${normal}\n"
	printf "\n"
sudo sh $SCRIPT_SUBDIR/app_dnf_rm.sh 2>&1 | su - "$SUDO_USER" -c "tee $LOGS/app_dnf_rm.log"
clear

# DNF: Add repositories
# Comment out for Silverblue
	printf "\n"
	printf "${cyan}Adding RPM repositories${normal}\n"
	printf "\n"
sudo sh $SCRIPT_SUBDIR/repo_dnf.sh 2>&1 | su - "$SUDO_USER" -c "tee $LOGS/repo_dnf.sh.log"
clear

# Flatpak: Add repositories
	printf "\n"
	printf "${cyan}Adding Flatpak repositories${normal}\n"
	printf "\n"
sudo sh $SCRIPT_SUBDIR/repo_flatpak.sh 2>&1 | su - "$SUDO_USER" -c "tee $LOGS/repo_flatpak.sh.log"
clear

# Update Fedora
# Comment out for Silverblue
	printf "\n"
	printf "${cyan}Updating the system${normal}\n"
	printf "\n"
sudo dnf upgrade -y --refresh 2>&1 | su - "$SUDO_USER" -c "tee $LOGS/dnf_upgrade.log"
clear

# Enable RPM Fusion                                               # Repository added via external script [scripts/repositories.sh]
# Comment out for Silverblue
	printf "\n"
	printf "${cyan}Installing RPM Fusion repository${normal}\n"
	printf "\n"
sudo sh $SCRIPT_SUBDIR/rpmfusion.sh 2>&1 | su - "$SUDO_USER" -c "tee $LOGS/rpmfusion.sh.log"
clear

# Enable HW video decode support for Intel integrated graphics    # Requires rpmfusion-free repository
#sudo sh $SCRIPT_SUBDIR/intel-gpu.sh                               # Uncomment line if you are using Intel integrated graphics

# DNF: Install applications
# Comment out for Silverblue
	printf "\n"
	printf "${cyan}Installing RPM applications${normal}\n"
	printf "\n"
sudo sh $SCRIPT_SUBDIR/app_dnf_in.sh 2>&1 | su - "$SUDO_USER" -c "tee $LOGS/app_dnf_in.sh.log"
clear

# Web browser: Install Microsoft Edge                             # Repository added via external script [scripts/repositories.sh]
# Comment out for Silverblue
	printf "\n"
	printf "${cyan}Installing Microsoft Edge${normal}\n"
	printf "\n"
sudo sh $SCRIPT_SUBDIR/app_msedge.sh 2>&1 | su - "$SUDO_USER" -c "tee $LOGS/app_msedge.sh.log"
clear

# Install custom shell commands
# Comment out for Silverblue	printf "\n"
	printf "${cyan}Adding custom shell commands${normal}\n"
	printf "\n"
sudo sh $SCRIPT_SUBDIR/shell_custom_cmd.sh 2>&1 | su - "$SUDO_USER" -c "tee $LOGS/shell_custom_cmd.log"
clear
