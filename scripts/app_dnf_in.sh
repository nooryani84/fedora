#!/bin/bash
#	set options explained:
#	'-e'		Stops the script if a command fails
#	'-u'		Any variable you haven't previously defined other than the special parameters ‘@’ or ‘*’
#			or array variables subscripted with ‘@’ or ‘*’ will cause an error that stops the script
#	'-x'            Causes bash to print each command before executing it.
#	'-o pipefail' 	If any command in a pipeline fails, the script stops
set -eo pipefail
#
################################################################################################################
# Define colors for output text
red=$(tput setaf 1)
normal=$(tput sgr0)

# Check if user is running script as root
if [ "$(id -u)" != "0" ]; then
   printf "${red}This script must be run as root.${normal}\n"
exit 1
fi
################################################################################################################
# if you want to remove gnome-software then you should first install gnome-firmware to avoid lots of dependencies being removed.
APP_DNF_IN="file-roller \
remmina \
gnome-shell-extension-appindicator \
steam-devices"

### Install applications from APP_DNF_IN defined at the top of script
sudo dnf install -y $APP_DNF_IN
