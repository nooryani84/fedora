#!/bin/bash
#	set options explained:
#	'-e'		Stops the script if a command fails
#	'-u'		Any variable you haven't previously defined other than the special parameters ‘@’ or ‘*’
#			or array variables subscripted with ‘@’ or ‘*’ will cause an error that stops the script
#	'-x'            Causes bash to print each command before executing it.
#	'-o pipefail' 	If any command in a pipeline fails, the script stops
set -eo pipefail
#
################################################################################################################
# Define colors for output text
red=$(tput setaf 1)
normal=$(tput sgr0)

# Check if user is running script as root
if [ "$(id -u)" != "0" ]; then
   printf "${red}This script must be run as root.${normal}\n"
exit 1
fi
################################################################################################################

APP_DNF_RM="firefox* \
gnome-boxes* \
gnome-calendar* \
gnome-classic-session* \
gnome-connections* \
gnome-contacts* \
gnome-clocks* \
gnome-maps* \
gnome-photos* \
gnome-weather* \
gnome-shell-extension-apps-menu* \
gnome-shell-extension-background-logo* \
gnome-shell-extension-common* \
gnome-shell-extension-launch-new-instance* \
gnome-shell-extension-places-menu* \
gnome-shell-extension-window-list* \
gst-editing-services* \
libreoffice* \
mediawriter* \
rhythmbox* \
simple-scan*"

# Remove applications from APP_DNF_RM defined at the top of script
sudo dnf remove -y $APP_DNF_RM
