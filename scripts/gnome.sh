#!/bin/bash
#	set options explained:
#	'-e'		Stops the script if a command fails
#	'-u'		Any variable you haven't previously defined other than the special parameters ‘@’ or ‘*’
#			or array variables subscripted with ‘@’ or ‘*’ will cause an error that stops the script
#	'-x'            Causes bash to print each command before executing it.
#	'-o pipefail' 	If any command in a pipeline fails, the script stops
set -eo pipefail
#
################################################################################################################
# Define colors for output text
red=$(tput setaf 1)
normal=$(tput sgr0)

# Check if user is running as root
if [ "$(id -u)" = "0" ]; then
    printf "${red}This script must be run as non-root.${normal}\n"
exit 1
fi
################################################################################################################

# Setting to switch font rendering to be more similar to GTK3
#tee -a ~/.config/gtk-4.0/settings.ini <<'EOF'
#[Settings]
#gtk-hint-font-metrics=1
#EOF

# Add right-click New File>Empty Document to GNOME Files
mkdir -p ~/Templates/
touch ~/Templates/Empty\ File

# Mouse speed - default value is '0'
#gsettings set org.gnome.desktop.peripherals.mouse speed -0.75

# Changes the mouse acceleration profile - default value is 'default'
gsettings set org.gnome.desktop.peripherals.mouse accel-profile 'flat'

# 'gsettings' can be obtained by executing "dconf watch /" and then manually changing settings
# To reset a specific key to its default value use 'gsettings reset'
# Eg: 'gsettings reset org.gnome.desktop report.technical-problems'
# 
# To list possible values for a key:
# Eg: 'gsettings range org.gnome.nautilus.preferences default-folder-viewer'
# will list all possible values for 'default-folder-viewer'
# Desktop
gsettings set org.gnome.desktop.wm.preferences audible-bell 'false'
gsettings set org.gnome.desktop.privacy report-technical-problems 'false'
gsettings set org.gnome.desktop.calendar show-weekdate 'true'
gsettings set org.gnome.desktop.interface enable-hot-corners 'false'

# Pin favorite apps to dash # find app launchers via folders /usr/share/applications/ /var/lib/flatpak/app/
gsettings set org.gnome.shell favorite-apps "['microsoft-edge.desktop', 'org.gnome.Nautilus.desktop']"

# GTK3
gsettings set org.gtk.Settings.FileChooser sort-directories-first 'true'

# GTK4
gsettings set org.gtk.gtk4.Settings.FileChooser sort-directories-first 'true'

# GNOME Software - disable automatic updates, but keep update notifications
gsettings set org.gnome.software download-updates 'false'

# Nautilus
gsettings set org.gnome.nautilus.preferences default-folder-viewer 'list-view'
gsettings set org.gnome.nautilus.list-view default-zoom-level 'small'
gsettings set org.gnome.nautilus.preferences show-delete-permanently 'true'
# gsettings set org.gnome.nautilus.preferences show-create-link 'true'

# Show line numbers in Text Editor
gsettings set org.gnome.TextEditor show-line-numbers 'true'
gsettings set org.gnome.TextEditor highlight-current-line 'true'
