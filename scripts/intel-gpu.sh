#!/bin/bash
#	set options explained:
#	'-e'		Stops the script if a command fails
#	'-u'		Any variable you haven't previously defined other than the special parameters ‘@’ or ‘*’
#			or array variables subscripted with ‘@’ or ‘*’ will cause an error that stops the script
#	'-x'            Causes bash to print each command before executing it.
#	'-o pipefail' 	If any command in a pipeline fails, the script stops
set -eo pipefail
#
################################################################################################################
# Define colors for output text
red=$(tput setaf 1)
normal=$(tput sgr0)

# Check if user is running script as root
if [ "$(id -u)" != "0" ]; then
   printf "${red}This script must be run as root.${normal}\n"
exit 1
fi
################################################################################################################
INTEL_GPU="libva \
libva-utils \
libva-intel-driver"

### Enable HW video decode support for Intel integrated graphics - requires rpmfusion-free repository
sudo dnf install -y $INTEL_GPU
