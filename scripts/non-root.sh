#!/bin/bash
#	set options explained:
#	'-e'		Stops the script if a command fails
#	'-u'		Any variable you haven't previously defined other than the special parameters ‘@’ or ‘*’
#			or array variables subscripted with ‘@’ or ‘*’ will cause an error that stops the script
#	'-x'            Causes bash to print each command before executing it.
#	'-o pipefail' 	If any command in a pipeline fails, the script stops
set -eo pipefail
#
################################################################################################################
# Define colors for output text
red=$(tput setaf 1)
normal=$(tput sgr0)

# Check if user is running as root
if [ "$(id -u)" = "0" ]; then
    printf "${red}This script must be run as non-root.${normal}\n"
exit 1
fi
################################################################################################################

REL_SCRIPT_SUBDIR="$(dirname $0)"
SCRIPT_SUBDIR="$(realpath $REL_SCRIPT_SUBDIR)"

# Flatpak: Remove applications
sh $SCRIPT_SUBDIR/app_flatpak_rm.sh

# Flatpak: Install applications
sh $SCRIPT_SUBDIR/app_flatpak_in.sh

# GNOME customizations
sh $SCRIPT_SUBDIR/gnome.sh
