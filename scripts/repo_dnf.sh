#!/bin/bash
#	set options explained:
#	'-e'		Stops the script if a command fails
#	'-u'		Any variable you haven't previously defined other than the special parameters ‘@’ or ‘*’
#			or array variables subscripted with ‘@’ or ‘*’ will cause an error that stops the script
#	'-x'            Causes bash to print each command before executing it.
#	'-o pipefail' 	If any command in a pipeline fails, the script stops
set -eo pipefail
#
################################################################################################################
# Define colors for output text
red=$(tput setaf 1)
normal=$(tput sgr0)

# Check if user is running script as root
if [ "$(id -u)" != "0" ]; then
    printf "${red}This script must be run as root.${normal}\n"
exit 1
fi
################################################################################################################

# Optimize DNF: Set maximum number of simultaneous package downloads. Defaults to 3. Maximum of 20.
echo "max_parallel_downloads=10" | sudo tee -a /etc/dnf/dnf.conf

# Enable free and nonfree RPM Fusion repositories [scripts/rpmfusion.sh]
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# Add repository for Microsoft Edge [scripts/app_msedge.sh]
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo dnf config-manager --add-repo https://packages.microsoft.com/yumrepos/edge
sudo mv /etc/yum.repos.d/packages.microsoft.com_yumrepos_edge.repo /etc/yum.repos.d/microsoft-edge.repo
