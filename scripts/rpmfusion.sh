#!/bin/bash
#	set options explained:
#	'-e'		Stops the script if a command fails
#	'-u'		Any variable you haven't previously defined other than the special parameters ‘@’ or ‘*’
#			or array variables subscripted with ‘@’ or ‘*’ will cause an error that stops the script
#	'-x'            Causes bash to print each command before executing it.
#	'-o pipefail' 	If any command in a pipeline fails, the script stops
set -eo pipefail
#
################################################################################################################
# Define colors for output text
red=$(tput setaf 1)
normal=$(tput sgr0)

# Check if user is running script as root
if [ "$(id -u)" != "0" ]; then
    printf "${red}This script must be run as root.${normal}\n"
exit 1
fi
################################################################################################################

# Repository added via external script [scripts/repositories.sh]

# AppStream metadata
# Enables users to install packages using Gnome Software/KDE Discover.
# Please note that these are a subset of all packages since the metadata are only generated for GUI packages.
sudo dnf groupupdate -y core

# Multimedia post-install
# The following command will install the complements multimedia packages needed by gstreamer enabled applications:
sudo dnf groupupdate -y multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin

# The following command will install the sound-and-video complement packages needed by some applications:
sudo dnf groupupdate -y sound-and-video

# Tainted repos
# Tainted free is dedicated for FLOSS packages where some usages might be restricted in some countries.
# Example: to play DVD with libdvdcss:
sudo dnf install -y rpmfusion-free-release-tainted
sudo dnf install -y libdvdcss

# Tainted nonfree is dedicated to non-FLOSS packages
sudo dnf install -y rpmfusion-nonfree-release-tainted
sudo dnf install -y \*-firmware

