#!/bin/bash
#	set options explained:
#	'-e'		Stops the script if a command fails
#	'-u'		Any variable you haven't previously defined other than the special parameters ‘@’ or ‘*’
#			or array variables subscripted with ‘@’ or ‘*’ will cause an error that stops the script
#	'-x'            Causes bash to print each command before executing it.
#	'-o pipefail' 	If any command in a pipeline fails, the script stops
set -eo pipefail
#
################################################################################################################
# Define colors for output text
red=$(tput setaf 1)
normal=$(tput sgr0)

# Check if user is running script as root
if [ "$(id -u)" != "0" ]; then
  printf "${red}This script must be run as root.${normal}\n"
exit 1
fi
################################################################################################################

# Restart function with user prompt

sudo tee /usr/local/bin/restart_prompt >/dev/null <<'EOF'
#!/bin/bash
set -e

# Sets colors for reboot message
red=$(tput setaf 1)
normal=$(tput sgr0)

# Restart function with user prompt
restart_prompt ()
{
printf "\n"
printf "#######################\n"
printf "# ${red}You should restart.${normal} #\n"
printf "#######################\n"
printf "\n"
printf "Would you like to restart now?\n"
printf "\n"

while true; do
read -p "(y/n)" choice
case $choice in
  y|Y ) systemctl reboot -i;;
  n|N ) break;;
esac
done
}
restart_prompt
EOF
sudo chmod +x /usr/local/bin/restart_prompt

################################################################################################################

# DNF upgrade function that downloads updates and installs them on next reboot

sudo tee /usr/local/bin/dnf-upgrade >/dev/null <<'EOF'
#!/bin/bash
set -e

# Check if user is running script as root
if [ "$(id -u)" != "0" ]; then
	printf "${red}This script must be run as root.\n"
    exit 1
fi

dnf-upgrade ()
  {
    export DNF_SYSTEM_UPGRADE_NO_REBOOT=1
# Download updates
      sudo dnf offline-upgrade -y download 
# Invoke offline-upgrade without rebooting. Upgrades will be performed next reboot.
      sudo -E dnf offline-upgrade reboot
}
dnf-upgrade
EOF

sudo chmod +x /usr/local/bin/dnf-upgrade
